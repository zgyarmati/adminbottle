#!/bin/bash


options='hn:d:'

usage(){
cat << EOF
usage: $0 options
 -h shows this message
 -d directory to create the class dir in
 -n the name of the plugin to create


EOF
}

while getopts $options option
do
    case $option in
        d )
          TARGETDIR=`readlink -f $OPTARG`
          ;;
        n )
          NAME=$OPTARG
          ;;
        h )
          usage
          exit 0
          ;;
    esac
done

echo "@ target dir: $TARGETDIR"
echo "@ plugin name: $NAME"


if [[ ! -d ${TARGETDIR} ]]
then
    echo "${TARGETDIR} doesnt exist"
    exit 1
fi


pushd  ${TARGETDIR}
CLASSDIR=${NAME}/${NAME} 
mkdir -p ${CLASSDIR}/assets ${CLASSDIR}/views

# creating a minimalistic plugin class implementation
cat > ${CLASSDIR}/${NAME}.py  << EOF
from bottle import *
from adminbottle.adminbottleplugin import *
import logging

PATH=os.path.dirname(os.path.realpath(__file__))
PLUGIN_VERSION=0.1


#the plugin class
class ${NAME}(AdminbottlePlugin):
    """ my new amazing adminbottle plugin"""

    def __init__(self, config=None):
        super(${NAME}, self).__init__(config)
        logging.debug("${NAME} plugin init")


    # this gives back the real content to the main site
    def getHtml(self):
        f = open(PATH + '/views/${NAME}_main.tpl', 'r')
        context = {}
        context['pluginname'] = self.pluginconfig['id']
        t = template(f.read(),context=context)
        f.close()
        return t
EOF

#creating a simple template
cat > ${CLASSDIR}/views/${NAME}_main.tpl  << EOF

<H1> This is your {{ context['pluginname']  }} plugin!</H1>

EOF


# creating an __init__ for the import
cat > ${CLASSDIR}/__init__.py  << EOF
__all__ = ["${NAME}"]
EOF


#creating setup.py for the deployment
cat > ${NAME}/setup.py << EOF
'''
    adminbottle plugin ${NAME} Setup
    ~~~~~~~~~~~~~~

    Package setup script.

    :copyright: Copyright 2011-2013 ...
    :license: FreeBSD, see LICENSE file
'''
from setuptools import setup, find_packages



long_desc = """adminbottle webui ${NAME} plugin"""


requires = ["bottle", 'configobj', 'adminbottle']
#test_requires = ['nose', 'tox']

setup(
    name = "${NAME}",
    version = 0.1,
    url = "http://",
    download_url = "http://pypi.python.org/bla",
    license = "FreeBSD",
    author = "Zoltan Gyarmati",
    author_email = "mr.zoltan.gyarmati@gmail.com",
    description = "adminbottle webui ${NAME} plugin",
    long_description = long_desc,
    classifiers = [
        "Development Status :: 5 - Production/Stable",
        "Environment :: Console",
        "Environment :: Web Environment",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Topic :: Communications",
        "Topic :: Internet"
    ],
    platforms = "any",
    packages = find_packages(),
    include_package_data = True,
    entry_points = {
        "adminbottle.plugin": [
            "${NAME} = ${NAME}.${NAME}:${NAME}"
        ],
    },
    install_requires = requires,
)
EOF


#creating Manifest.in
cat > ${NAME}/MANIFEST.in << EOF
include COPYING.lgpl

recursive-include ${NAME} *
EOF


cat << EOF

    your plugin is created in ${TARGETDIR}/${NAME}
    to test it, add this lines to your config, in the [MENU] section:

        [[${NAME}-config]]
          plugin = ${NAME}
          enabled = True
          display_name = ${NAME}-configuration


EOF
