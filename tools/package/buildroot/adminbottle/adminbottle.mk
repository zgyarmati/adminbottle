################################################################################
#
# adminbottle
#
################################################################################

ADMINBOTTLE_VERSION = HEAD
ADMINBOTTLE_SITE = git@bitbucket.org:zgyarmati/adminbottle.git
ADMINBOTTLE_SITE_METHOD = git
ADMINBOTTLE_DEPENDENCIES = python python-bottle python-configobj python-setuptools
HOST_ADMINBOTTLE_DEPENDENCIES = host-python-setuptools
ADMINBOTTLE_LICENSE = LGPL

define ADMINBOTTLE_BUILD_CMDS
	(cd $(@D); $(HOST_DIR)/usr/bin/python setup.py build --executable=/usr/bin/python)
endef

define ADMINBOTTLE_INSTALL_TARGET_CMDS
	(cd $(@D); PYTHONPATH=$(TARGET_DIR)/usr/lib/python$(PYTHON_VERSION_MAJOR)/site-packages \
     $(HOST_DIR)/usr/bin/python setup.py install --prefix=$(TARGET_DIR)/usr)
     sed -i '1s/.*/\#!\/usr\/bin\/python/' $(TARGET_DIR)/usr/bin/adminbottle
endef


define ADMINBOTTLE_INSTALL_INIT_SYSV
	@echo "INSTALL INIT SCRIPT FOR ADMINBOTTLE `pwd`"
	@cp package/adminbottle/S89adminbottle $(TARGET_DIR)/etc/init.d/
endef


#ifeq ($(ADMINBOTTLE_INSTALL_INITSCRIPT),y)
# TARGETS+=adminbottleinit
#endif
$(eval $(generic-package))
