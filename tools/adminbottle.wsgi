from adminbottle.adminbottle import default_app

application = default_app()


"""
<VirtualHost *:80>
    ServerName localhost

    WSGIDaemonProcess adminbottle user=www-data group=www-data processes=1 threads=5
    WSGIScriptAlias / /var/www/adminbottle.wsgi

    <Directory /var/www>
        WSGIProcessGroup adminbottle
        WSGIApplicationGroup %{GLOBAL}
        Order deny,allow
        Allow from all
    </Directory>
</VirtualHost>
"""
