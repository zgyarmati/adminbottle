'''
    Adminbottle setup
    ~~~~~~~~~~~~~~
    Package setup script.
'''
from setuptools import setup, find_packages

long_desc = '''
adminbottle webui config
'''

requires = ["bottle", 'configobj']

setup(
    name = "Adminbottle",
    version = 0.1,
    url = "http://",
    download_url = "http://pypi.python.org/bla",
    license = "FreeBSD",
    author = "Zoltan Gyarmati",
    author_email = "mr.zoltan.gyarmati@gmail.com",
    description = "adminbottle webui config",
    long_description = long_desc,
    classifiers = [
        "Development Status :: 5 - Production/Stable",
        "Environment :: Console",
        "Environment :: Web Environment",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Topic :: Internet"
    ],
    platforms = "any",
    packages = find_packages(exclude=["adminbottletest"]),
    include_package_data = True,
    entry_points = {
        "console_scripts": [
            "adminbottle = adminbottle.adminbottle:main"
        ],
        "adminbottle.plugin": [
            "logtail = adminbottle.plugins.logtail.logtail:logtail",
            "home = adminbottle.plugins.home.home:home",
            "embedder = adminbottle.plugins.embedder.embedder:embedder"
        ]
    },
    install_requires = requires,
)
