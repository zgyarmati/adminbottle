% rebase('base.tpl', header_content=header_content, loggedin=loggedin, title = title)
<div class=configscreen>
   <div class=menu>
        <h2>Menu</h2>
        <ul class="menulist">
            %for element in menus:
              <li class="file"><a href="?plugin={{ element['id'] }}">{{ element['display_name'] }}</a></li>
            %end
        </ul>
   </div>
   <div class=content>
      %if has_apply:
        <form action="/?plugin={{active_plugin}}" method="post">
            <div class="plugincontent">
            {{!plugin_content}}
            </div>

            <div class="buttongroup">
            <input name="cancel" type="submit" value="Cancel"/>
            <input name="apply" type="submit" value="Apply changes"/>
            </div>
        </form>
      %else:
        {{!plugin_content}}
      %end

   </div>
</div>
