<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en-GB">

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" type="text/css" href="static/base.css" media="screen">
    <title>{{ title }}</title>
</head>

<body>
    <div class="banner">
        <div class="bannertext">
              <a href="/">{{ header_content }}</a>
        </div>

        <div  class="statusline">
        %if loggedin:
            <a href="/change_password">Change admin password</a>
            <a href="/logout">Logout</a>
        %end
        </div>
    </div>
    %include
    <!--div class=footer>
    Adminbottle, Copyright &copy; 2013
    </div-->
</body>
</html>

