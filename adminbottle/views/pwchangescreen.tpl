% rebase('base.tpl', header_content=header_content, loggedin=loggedin)

<div class="box">
    <h2>Admin password change</h2>
    <p>Please insert your old and new admin passwords:</p>
    <form action="/change_password" method="post">
        <input type="password" name="old_password" placeholder="Old password"/>
        <input type="password" name="new_password"placeholder="New password" />
        <input type="password" name="new_password_repeat" placeholder="Confirm new password"/>

        <br/><br/>
        <button type="submit" > OK </button>
        <button type="button" class="close"> Cancel </button>
    </form>
    <br />
</div>
