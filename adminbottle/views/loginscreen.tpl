% rebase('base.tpl', header_content=header_content, loggedin=loggedin,title=title)

  <div class="box">
      <h2>Login</h2>
      <p>Please insert your credentials:</p>
      <form action="login" method="post" name="login">
          <input type="text" name="username" />
          <input type="password" name="password" />

          <br/><br/>
          <button type="submit" > OK </button>
          <button type="button" class="close"> Cancel </button>
      </form>
      <br />
  </div>
