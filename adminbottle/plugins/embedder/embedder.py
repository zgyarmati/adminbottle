# This file is part of adminbottle, a simple web configuration framework
# based on the bootle.py micro web framework.
# Copyright Zoltan Gyarmati <mr.zoltan.gyarmati[at]gmail.com> 2013
#
#
# This software is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 2.1, as published by the Free Software Foundation.
#
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this sofware; see the file COPYING.lgpl.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.

from bottle import *
from datetime import timedelta
from time import gmtime, strftime
import subprocess
import logging

PATH=os.path.dirname(os.path.realpath(__file__))
PLUGIN_VERSION=0.1


#the plugin class
class embedder:
    """A simple plug-in to embed external web-service into adminbottle"""
    def __init__(self, config=None):
        logging.debug ("embedder config: %s",str(config))
        self.pluginconfig = config
    
    
    # this gives back the real content to the main site
    def getHtml(self):
        f = open(PATH + '/views/embedder_main.tpl', 'r')
        context = {}
        context['url']= self.pluginconfig['url']
        t = template(f.read(),context=context)
        return t
