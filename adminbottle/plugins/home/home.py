# This file is part of adminbottle, a simple web configuration framework
# based on the bootle.py micro web framework.
# Copyright Zoltan Gyarmati <mr.zoltan.gyarmati[at]gmail.com> 2013
#
#
# This software is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 2.1, as published by the Free Software Foundation.
#
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this sofware; see the file COPYING.lgpl.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.


from bottle import *
from datetime import timedelta
from time import gmtime, strftime
import subprocess
from adminbottle.adminbottleplugin import *
import os
from collections import namedtuple
import math

PATH=os.path.dirname(os.path.realpath(__file__))
PLUGIN_VERSION=0.1
usage_ntuple = namedtuple('usage',  'total used free percent')

def convertSize(size):
   size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(size,1024)))
   p = math.pow(1024,i)
   s = round(size/p,2)
   if (s > 0):
       return '%s %s' % (s,size_name[i])
   else:
       return '0B'

def diskPartitions(all=False):
    """Return all mountd partitions as a nameduple.
    If all == False return phyisical partitions only.
    """
    phydevs = []
    f = open("/proc/filesystems", "r")
    for line in f:
        if not line.startswith("nodev"):
            phydevs.append(line.strip())

    retlist = []
    f = open('/etc/mtab', "r")
    for line in f:
        partinfo = {}
        if not all and line.startswith('none'):
            continue
        fields = line.split()
        device = fields[0]
        mountpoint = fields[1]
        fstype = fields[2]
        if not all and fstype not in phydevs:
            continue
        if device == 'none':
            device = ''
        partinfo['dev'] = fields[0]
        partinfo['path'] = fields[1]
        partinfo['fs'] = fields[2]

        usage = diskUsage(fields[1])
        partinfo['total'] = usage[0]
        partinfo['used'] = usage[1]
        partinfo['free'] = usage[2]
        partinfo['perc'] = usage[3]
        retlist.append(partinfo)
    return retlist

def diskUsage(path):
    """Return disk usage associated with path."""
    st = os.statvfs(path)
    free = (st.f_bavail * st.f_frsize)
    total = (st.f_blocks * st.f_frsize)
    used = (st.f_blocks - st.f_bfree) * st.f_frsize
    try:
        percent =  (float(used) / total) * 100
    except ZeroDivisionError:
        percent = 0
    # NB: the percentage is -5% than what shown by df due to
    # reserved blocks that we are currently not considering:
    # http://goo.gl/sWGbH
    return usage_ntuple(convertSize(total), convertSize(used), convertSize(free), round(percent, 1))



#some helper functions
def getSysUptime():
    with open('/proc/uptime', 'r') as f:
        uptime_seconds = float(f.readline().split()[0])
        uptime_string = str(timedelta(seconds = uptime_seconds))
    return uptime_string


def getSysTime():
    return strftime("%Y-%m-%d %H:%M:%S", gmtime())

def getNetworkInfos():
    """gives back a list of the interfaces, and their ip and netmask"""
    #  TODO move to netifaces?
    p = subprocess.Popen("/sbin/ifconfig", stdout=subprocess.PIPE)
    result = p.communicate()[0]
    return re.findall(r'^(\S+).*?inet addr:(\S+).*?Mask:(\S+)',
                    result, re.S | re.M)

def getFileSystemInfos():
    pass


class home(AdminbottlePlugin):
    """A simple home plugin, which shows system data overview"""

    def __init__(self, config=None):
        super(home, self).__init__(config)
        self.register_handler('reboot',self.doReboot)

    def getHtml(self):
        f = open(PATH + '/views/home_main.tpl', 'r')
        context = {}
        context['pluginid'] = self.pluginconfig['id']
        context['uptime']= getSysUptime()
        context['time']=  getSysTime()
        context['hostname'] = subprocess.check_output(["uname", "-n",])
        context['kernelversion'] = subprocess.check_output(["uname", "-r",])
        context['interfaces'] = getNetworkInfos()
        context['fsinfos'] = diskPartitions()
        t = template (f.read(),context=context)
        f.close()
        return t


    def doReboot(self, request):
        """simply calls the reboot command, confirmation
        and notification is up to HTML/JS"""
        os.system("reboot")
