# This file is part of adminbottle, a simple web configuration framework
# based on the bootle.py micro web framework.
# Copyright Zoltan Gyarmati <mr.zoltan.gyarmati[at]gmail.com> 2013
#
#
# This software is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 2.1, as published by the Free Software Foundation.
#
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this sofware; see the file COPYING.lgpl.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.

from bottle import *
from datetime import timedelta
from time import gmtime, strftime
import subprocess
import logging
from adminbottle.adminbottleplugin import *

PATH=os.path.dirname(os.path.realpath(__file__))
PLUGIN_VERSION=0.1

# FIXME maybe a faster implementation here?
# or maybe even merge the getLineNum to this fun?
def tail( path, n=20 ):
    # return the last N lines from the file indicated in path
    logging.debug('requested last lines: %d', n)
    fp = open(path) # open the file for seeking ahead
    back = open(path) # open the file to read the needed lines
    retval = ''

    for i in range(int(n)):
        fp.readline()
    for each in fp:
        back.readline()
    for line in back:
        retval += line

    return retval


def getLineNum(fname):
    with open(fname) as f:
        retval = -1
        for retval, l in enumerate(f):
            pass
    return retval + 1

#the plugin class
class logtail(AdminbottlePlugin):
    """A simple log viewer plugin"""

    def __init__(self, config=None):
        super(logtail, self).__init__(config)
        #for the ajax request to get the delta
        self.register_handler('getlines',self.getNewLines)
        #to download the logfile
        self.register_handler('getfile',self.serveFile)

    # this gives back the real content to the main site
    def getHtml(self):
        f = open(PATH + '/views/logtail_main.tpl', 'r')
        logfilepath = self.pluginconfig['logfile']
        context = {}
        context['logfile']= logfilepath
        context['linenum'] = self.pluginconfig['linenum']
        context['pluginid'] = self.pluginconfig['id']
        linenum = int(self.pluginconfig['linenum'])
        fulllinenum = getLineNum(logfilepath)
        if linenum > fulllinenum:
            i = fulllinenum
        else:
            i = linenum
        try:
            context['initial'] =  tail(logfilepath, i)
            context['fulllinenum'] = fulllinenum
            context['filesize'] = os.path.getsize(logfilepath)
        except IOError:
            context['initial'] = "ERROR: File not found!"
            context['fulllinenum'] = 0
            context['filesize'] = 0
        t = template(f.read(),context=context)
        return t

    def getNewLines(self, request):
        p_size = request.query.previous_size
        p_logfile= self.pluginconfig['logfile']
        new_size = 0
        logging.debug ('previous size: %s', p_size)
        logging.debug ('requested logfile: %s', p_logfile)
        try:
            new_size = getLineNum(p_logfile)
        except IOError:
            retval = 'ERROR: File not found'
        response.add_header('size', new_size)
        if int(new_size) > int(p_size):
            try:
                retval = tail(p_logfile, new_size - int(p_size))
            except IOError:
                retval = 'ERROR: File not found'
            return retval

        else:
            return ''


    def serveFile(self, request):
            return static_file(self.pluginconfig['logfile'], root='/',
            mimetype='application/octet-stream')
