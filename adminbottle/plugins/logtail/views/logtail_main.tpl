
<H1>Watching logfile: <i>{{context['logfile']}} </i></H1>
</br>
</br>
<textarea name="textarea" id="logviewer_area" rows="{{context['linenum']}}"
style="width:99%;"> {{context['initial']}}</textarea>

<FORM NAME="updatesettings" ACTION="" METHOD="GET">
<INPUT TYPE="button" id="pausebutton" value="Pause" onClick="pauseStart(this)">
<INPUT TYPE="button" id="clearbutton" value="Clear" onClick="clearText(this)">
Update interval:
<select id="timeval_combobox" onchange='updateTimer(this)' >
  <option value="1">1 sec</option>
  <option value="3">3 sec</option>
  <option value="5">5 sec</option>
  <option value="10">10 sec</option>
</select>
<a href="/plugins/{{context['pluginid']}}/getfile">Download logfile</a>
</FORM>
<script type="text/javascript">
textsize = {{context['fulllinenum']}};
interval_id = 0
setUp()

function setUp(){
    interval_id = setInterval(function(){getNewLines()},1000);
}

function clearText(btn){
    textarea = document.getElementById("logviewer_area")
    textarea.value = '';
}

function pauseStart(btn){
    var btn_value = btn.value
    sel = document.getElementById("timeval_combobox");

    if (btn_value == 'Pause'){
        clearInterval(interval_id);
        sel.disabled=true;
        btn.value= "Start"
    }
    else{
        var value = parseInt(sel.options[sel.selectedIndex].value);
        sel.disabled=false;
        interval_id = setInterval(getNewLines(),1000 * value);
        btn.value= "Pause"
    }
}

function updateTimer(sel){

    var value = parseInt(sel.options[sel.selectedIndex].value);
    clearInterval(interval_id);
    interval_id = setInterval(getNewLines(),1000 * value);
}

function getNewLines(){
    var http = new XMLHttpRequest();
    var url = "/plugins/{{context['pluginid']}}/getlines";
    var params = "previous_size=" + textsize;
    http.open("GET", url+"?"+params, true);
    http.onreadystatechange = function() {//Call a function when the state changes.
        if(http.readyState == 4 && http.status == 200) {
            new_textsize = parseInt(http.getResponseHeader("size"))
            if (new_textsize < textsize){
                clearText(0)
            }
            textsize = new_textsize
            textarea = document.getElementById("logviewer_area")
            if (http.responseText.length > 1){
                textarea.value += http.responseText;
                textarea.scrollTop = textarea.scrollHeight;
            }
        }
    }//function
    http.send(null);
} //getNewLines
</script>


