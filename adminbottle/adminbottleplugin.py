# This file is part of adminbottle, a simple web configuration framework
# based on the bootle.py micro web framework.
# Copyright Zoltan Gyarmati <mr.zoltan.gyarmati[at]gmail.com> 2013
#
#
# This software is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 2.1, as published by the Free Software Foundation.
#
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this sofware; see the file COPYING.lgpl.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.
from bottle import *


class AdminbottlePlugin(object):
    def __init__(self, config=None):
        self.pluginconfig = config
        self.urlhandlers = {}


    def get_html(self):
        redirect('/errorscreen')

    def register_handler(self, url, fun):
        self.urlhandlers[url] = fun


    def dispatch_url(self, path, request):
        if path in self.urlhandlers:
            return self.urlhandlers[path](request)
        redirect('/errorscreen')
