# -*- coding: UTF-8 -*-
# This file is part of adminbottle, a simple web configuration framework
# based on the bootle.py micro web framework.
# Copyright Zoltan Gyarmati <mr.zoltan.gyarmati[at]gmail.com> 2013
#
#
# This software is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 2.1, as published by the Free Software Foundation.
#
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this sofware; see the file COPYING.lgpl.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.
#
# based on https://github.com/bbrodriges/bottlepy-user-auth

from bottle import request, response,redirect
import time
import csv
import os
import hashlib
import uuid
import logging


class UserDb():
    def __init__ (self, app):
        self.pw_filename = app.config['pwfile']
    def getHashByUser(self,username):
        logging.debug('getting hash password of user: %s', username)

        with open(self.pw_filename, 'rb') as f:
            reader = csv.reader(f, delimiter=':', quoting=csv.QUOTE_NONE)
            for row in reader:
                if row[0] == username:
                    return row[1]
            return None

    def getHashByUid(self ,uid):
        logging.debug('getting hash password of Uid: %s', uid)

        with open(self.pw_filename, 'rb') as f:
            reader = csv.reader(f, delimiter=':', quoting=csv.QUOTE_NONE)
            for row in reader:
                if row[2] == uid:
                    return row[1]
            return None


    def getUidByUser(self,username):
        logging.debug ('gettint Uid for user: %s', username)

        with open(self.pw_filename, 'rb') as f:
            reader = csv.reader(f, delimiter=':', quoting=csv.QUOTE_NONE)
            for row in reader:
                if row[0] == username:
                    return row[2]
            return None

    def getUserByUid(self,uid):
        logging.debug('getting Username for uid: %s', uid)

        with open(self.pw_filename, 'rb') as f:
            reader = csv.reader(f, delimiter=':', quoting=csv.QUOTE_NONE)
            for row in reader:
                if row[2] == uid:
                    return row[0]
            return None

    def createHash(self,password):
        print 'creating hash from pw:', password
        m = hashlib.md5()
        m.update(password)
        pwd_md5 = m.hexdigest()
        print str(pwd_md5)
        return pwd_md5

    def changePwHash(self,user,new_pw):
        logging.debug('change pw hash for user: %s', user)
        pw_md5 = self.createHash (new_pw)

        with open(self.pw_filename, 'rb') as f_in:
            with open(self.pw_filename + '_temp', 'w') as f_out:
                reader = csv.reader(f_in, delimiter=':', quoting=csv.QUOTE_NONE)
                writer = csv.writer(f_out, delimiter=':', quoting=csv.QUOTE_NONE)
                for row in reader:
                    if row[0] == user:
                        row[1] = pw_md5
                    writer.writerow(row)
        os.rename(self.pw_filename + '_temp', self.pw_filename)

    def addUser(self,user,pw):
        if len(user) and len(pw):
            if getHashByUser(user):
                logging.warning('trying to add existing user, canceling...')
                return
            pw_md5 = self.createHash(pw)
            with open(self.pw_filename, 'a+') as f:
                writer = csv.writer(f, delimiter=':', quoting=csv.QUOTE_NONE)
                row = []
                row.append(  user)
                row.append (pw_md5)
                row.append (str(uuid.uuid4()))
                writer.writerow(row)

class User:

    def __init__( self, dbpath):
        logging.debug ("creating user __init__()")
        self.COOKIE_SECRET_KEY = 'my_very_secret_key' #change this key to yours
        self.loggedin = False
        self.credentials = None
        self.username = None
        self.db = UserDb(dbpath)
        self.validate() #validating user to see if he is logged in

    def authenticate( self , username , password ):
        """
            @type username str
            @type password dict
            Checks user credentials and authenticates him in system.
        """
        logging.debug('>>> authenticate: username: %s, password: %s', username,password)
        if username and password:
         pw_hash = self.db.getHashByUser(username)
         if not pw_hash:
            print 'non-existing user...'
            return False
         uid = self.db.getUidByUser(username)
         if pw_hash == self.db.createHash(password):
                print "na, login"
                self.set_cookie(self.db.getUidByUser(username))
                self.loggedin = True
                self.credentials = uid
                self.username = username
                return True

         return False

    def logout( self ):
        """Initiates user logout by destoying cookie.
        """
        self.remove_cookie()
        self.loggedin = False
        self.credentials = None
        return True


    def register( self , username , password ):
        """@type username str
           @type password str

           Get user, password checks if username is already
           registered, hashes password with md5 and store
           user data."""
        if username and password:
            if self.db.getHashByUser(username):
              print 'already existing user...'
              return False
            addUser (username, password)
            return True

    def change_pw(self, new_password):
        """changes the password of the current user"""
        self.db.changePwHash(self.db.getUserByUid(self.credentials),new_password)
        self.logout()


    def validate(self):
        """Validates user credential by decrypting encrypted cookie.
        Indicates that user is logged in and verified. If verification
        fails - destroys cookie by calling logout method ( because of
        possible cookie fraud ). Stores user info in credentials
        attribute in case of successful decryption."""

        logging.debug('try to validate')
        uid = request.get_cookie( '__utmb' , secret = self.COOKIE_SECRET_KEY )
        print 'uid:' , uid
        if not uid:
            return None
        username = self.db.getUserByUid(uid)
        if username:
            logging.debug ("validate: succesful for: %s", username)
            self.loggedin = True
            self.credentials = uid
            self.username = username
            return True

        logging.debug('validate: non-existing user...')
        self.logout()
        return None


    #COOKIES
    def set_cookie(self,uid):
        """Sets user cookie based on his uid."""
        response.set_cookie(
                '__utmb',
                uid,
                secret = self.COOKIE_SECRET_KEY,
                expires = time.time() + ( 3600*24 ),
                path = '/'
        )

    def remove_cookie( self ):
        """Destroys user cookie."""
        response.set_cookie(
                '__utmb',
                '',
                secret = self.COOKIE_SECRET_KEY,
                expires = time.time() - ( 3600*24 ),
                path = '/'
        )


    @staticmethod
    def require_login(app):
       """decorator for functions require login, it needs the pass files path
       as an argument"""
       def wrap(f):
            def check_uid(*args, **kwargs):
                pwfile = app.config['pwfile']
                user = User(app)  # here and below - creates an instance of User class
                if user.loggedin:
                    return f(*args, **kwargs)
                else:
                    redirect("/login")
            return check_uid
       return wrap
