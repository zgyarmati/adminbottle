#!/usr/bin/python
# This file is part of adminbottle, a simple web configuration framework
# based on the bootle.py micro web framework.
# Copyright Zoltan Gyarmati <mr.zoltan.gyarmati[at]gmail.com> 2013
#
#
# This software is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 2.1, as published by the Free Software Foundation.
#
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this sofware; see the file COPYING.lgpl.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.

import bottle
from bottle import *
from os import path
import importlib
import getopt
import logging, logging.handlers
import sys, atexit, os, pwd
#auth stuff
from adminbottleauth import User
#config
from adminbottleconfig import AdminbottleConfig
# used for plugin management
from pkg_resources import iter_entry_points

DEFAULT_CONFIGFILE = '/etc/adminbottle.ini'
PIDFILE = '/var/run/adminbottle.lock'

#  general global variables
PATH=os.path.dirname(os.path.realpath(__file__)) + os.sep
themespath = os.path.join(PATH + 'themes')
bottle.TEMPLATE_PATH.insert(0,os.path.join(PATH + 'views'))

app = bottle.app()


# Login form and post
@bottle.post('/login')
def post_login():
    """Authenticate users"""
    user = User(app)
    if user.authenticate(bottle.request.POST.get('username'),
                         bottle.request.POST.get('password')):
        bottle.redirect('/')
    bottle.redirect('/login')


@bottle.route('/login')
@bottle.view('loginscreen')
def get_login():
    """Serve login form, or redirect to main view, if user logged in already"""
    user = User(app)  # here and below - creates an instance of User class
    if user.loggedin:
        bottle.redirect('/')
    context = app.config['context']
    context['loggedin'] = False
    print '------------'
    print context
    return context


# logging out
@bottle.route('/logout')
def logout():
    user = User(app)  # here and below - creates an instance of User class
    if user.loggedin:
        logging.debug ('You are an authenticated user, but now you log out')
        user.logout()
    bottle.redirect('/login')


# password change view and post
@bottle.route('/change_password')
@User.require_login(app)
@bottle.view('pwchangescreen')
def get_change_password():
    """Show password change form"""
    context = app.config['context']
    context['loggedin'] = True
    return context


@bottle.route('/errorscreen')
def errorscreen():
    return "ERROR!"


@bottle.route('/error')
@bottle.view('errorscreen')
def error(msg, link):
    context = app.config['context']
    context['loggedin'] = True
    context['errormessage'] = msg
    context['retrylink'] = link
    return context

@bottle.post('/change_password')
def post_change_password():
    """Change password"""
    # here we cant use the require_login() decorator,
    # as we need the user object
    user = User(app)
    if not user.loggedin:
        bottle.redirect('/login')
        return

    new_pw = bottle.request.POST.get('new_password')
    new_pw_r = bottle.request.POST.get('new_password_repeat')
    print 'PASSWORDS', new_pw, new_pw_r
    if new_pw != new_pw_r:
        return error('The repeated password doesn\'t match', '/change_password')

    old_pw = bottle.request.POST.get('old_password')
    if not user.authenticate(user.username, old_pw):
        return error('The old password doesn\'t match', '/change_password')

    user.change_pw(bottle.request.POST.get('new_password'))
    return 'Password changed <a href="/login">Go to login</a>'


# main content view and post
@bottle.route('/')
@User.require_login(app)
@bottle.view('configscreen')
def serve_mainview():

    active_plugin = bottle.request.query.plugin
    if not active_plugin:
        active_plugin = app.config['default_plugin']
    # creating the plugin based by the given name:
    pconf = app.config['config'].getPluginConfig(active_plugin)
    try:
        p = app.config['plugin_constructors'][active_plugin](pconf)
    except:
        logging.error('Failed to load plugin: ' + active_plugin)
        logging.error(sys.exc_info())
        return error('Failed to load plugin: ' + active_plugin, '/')

    # creating and filling the context object
    context = app.config['context']
    context['active_plugin'] = active_plugin
    try:
        context['plugin_content'] = p.getHtml()
    except:
        logging.error('Failed to load plugin: ' + active_plugin)
        logging.error(sys.exc_info())
        return error('Failed to load plugin: ' + active_plugin, '/')
    context['loggedin'] = True
    # if the plugin can save the config, we also
    # put a nice Apply button on the page
    if hasattr(p, 'saveConfig'):
        context['has_apply'] = True
    else:
        context['has_apply'] = False
    return context


@bottle.route('/', method='POST')
@User.require_login(app)
def processPost():
    logging.debug ("process post action....")
    logging.debug(request.forms.get('cancel'))
    active_plugin = request.query.plugin
    # creating the plugin based by the given name:
    # and save the new target config
    pconf = app.config['config'].getPluginConfig(active_plugin)
    p = app.config['plugin_constructors'][active_plugin](pconf)
    p.saveConfig(request)

    redirect('/')


#serving plugins' own custom stuff
@bottle.route('/plugins/<plugin>/<path:path>',method='POST')
@bottle.route('/plugins/<plugin>/<path:path>',method='GET')
@User.require_login(app)
def serve_plugin(plugin,path):
    # creating the plugin based by the given name:
    pconf = app.config['config'].getPluginConfig(plugin)
    p = app.config['plugin_constructors'][plugin](pconf)
    # creating and filling the context object
    context = app.config['context']
    return p.dispatch_url(path, request)


#serving general static files
@bottle.route('/static/<path:path>')
def serve_static_file(path):
    return static_file(path, app.config['themedir'])


#serving plugin static files (assets)
@bottle.route('/<plugin>/assets/<path:path>')
@User.require_login(app)
def serve_plugin_asset(plugin, path):
        pluginpath = os.path.dirname(sys.modules['adminbottle.plugins.'+ plugin].__file__)
        p = str(pluginpath + '/assets/')
        return static_file(path, p)

def print_usage():
      print 'adminbottle -c <configfile> -n'
      print '-c set configfile to use, default/etc/adminbottle.ini'
      print '-n no deamonize the process'

def is_process_running(process_id):
    try:
        os.kill(process_id, 0)
        return True
    except OSError:
        return False

def delpid():
    os.remove(PIDFILE)

def handle_sigterm(*args):
    sys.exit(0)


def daemonize():
        ''' Daemonize the current process and return '''
        try:
            pf = file(PIDFILE,'r')
            pid = int(pf.read().strip())
            pf.close()
        except:
            pid = None
        if pid:
            sys.stderr.write('pidfile exists, the pid: ' + str(pid) + '\n')
            if is_process_running(pid):
                sys.stderr.write('other instance is still running, exiting...\n')
                sys.exit(1)
            else:
                sys.stderr.write('it seems there is a leftover pid file, deleting it\n')
                delpid()
        try:
            pid = os.fork()
            if pid > 0:
                # exit first parent
                sys.exit(0)
        except OSError, e:
            return 1
        # decouple from parent environment
        os.setsid()
        os.umask(0)
        # do second fork
        try:
            pid = os.fork()
            if pid > 0:
                # exit from second parent
                sys.exit(0)
        except OSError, e:
            return 1
        # redirect standard file descriptors
        sys.stdout.flush()
        sys.stderr.flush()

        #si = file('/dev/null', 'r')
        #so = file('/dev/null', 'a+')
        #se = file('/dev/null', 'a+', 0)
        #os.dup2(si.fileno(), sys.stdin.fileno())
        #os.dup2(so.fileno(), sys.stdout.fileno())
        #os.dup2(se.fileno(), sys.stderr.fileno())
        #signal.signal(signal.SIGTERM, handle_sigterm)

        # write pidfile
        atexit.register(delpid)
        pid = str(os.getpid())
        file(PIDFILE,'w+').write("%s\n" % pid)


def main():
    global app
    daemon = True
    configfile = DEFAULT_CONFIGFILE

    try:
        opts, args = getopt.getopt(sys.argv[1:],"hnc:",["help","nodaemon" ,"configfile="])
    except getopt.GetoptError:
        print_usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print_usage()
            sys.exit()
        elif opt in ("-c", "--configfile"):
            configfile = arg
        elif opt in ("-n", "--no-deamon"):
            daemon = False

    ## some base sanity tests
    if not os.path.exists(configfile):
        print ("Fatal: Config file doesn't exist: %s", configfile)
        exit(1)

    try:
        conf = AdminbottleConfig(configfile)
    except:
        print ("Fatal: problem with loading initial config")
        sys.exit(1)

    if not os.path.exists(conf.getGeneralValue('shadowfile')):
        print ("Fatal: password file doesn't exist: %s", filename)
        sys.exit(1)

    if daemon:
        daemonize();

    ## setting up logging
    num_loglevel = getattr(logging, conf.loglevel.upper(), None)
    if not isinstance(num_loglevel, int):
        raise ValueError('Invalid log level: %s' % loglevel)
    if daemon:
        logging.basicConfig(level=num_loglevel,filename=conf.logfile,
                            format='%(asctime)s %(message)s')
    else:
        logging.basicConfig(level=num_loglevel,format='%(asctime)s %(message)s')


    ## filling up the context with the permanent values
    ## this will be passed at every template render
    context = {}
    context['header_content'] = conf.getGeneralValue('headertext')
    context['title'] = conf.getGeneralValue('html title')


    enabled_plugins = conf.getEnabledPlugins()
    context['menus'] = enabled_plugins

    # creating a dict with the constructors of the plugins,
    # mapped to the real names and id-s of the plugins
    plugin_constructors = {}
    for entryobject in iter_entry_points(group='adminbottle.plugin', name=None):
        for p in enabled_plugins:
            name = p['plugin']
            if name == entryobject.name:
                logging.info("register constructor for: %s", entryobject.name)
                fun = entryobject.load()
                plugin_constructors[p['id']] = fun
    app.config['plugin_constructors'] = plugin_constructors
    app.config['themedir'] = os.path.join(themespath, conf.getGeneralValue('theme'))
    app.config['config'] = conf
    app.config['context'] = context
    app.config['default_plugin'] = conf.getGeneralValue('default plugin')
    app.config['themedir'] = os.path.join(themespath, conf.getGeneralValue('theme'))
    app.config['pwfile'] = conf.getGeneralValue('shadowfile')

    logging.info("Start adminbottle bottle.py server")
    bottle.debug(True)
    bottle.run(app=app, quiet=False, reloader=False,
    host = conf.getGeneralValue('listening address'),
    port = conf.getGeneralValue('serverport'))
