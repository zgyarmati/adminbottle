# This file is part of adminbottle, a simple web configuration framework
# based on the bootle.py micro web framework.
# Copyright Zoltan Gyarmati <mr.zoltan.gyarmati[at]gmail.com> 2013
#
#
# This software is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 2.1, as published by the Free Software Foundation.
#
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this sofware; see the file COPYING.lgpl.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.

from configobj import ConfigObj
import logging

class AdminbottleConfig:
    def __init__(self,filename):
        # no logging yet
        print ('init config')
        try:
            self.configobj  = ConfigObj(filename)
            self.logfile = self.configobj['General']['logfile']
            self.loglevel = self.configobj['General']['loglevel']
        except:
            raise



    def getEnabledPlugins(self):
        """returns a list with the currently enabled
        plugins. Each plugin is a dict"""
        retval = []
        c = self.configobj['Menu']
        for item in c:
            if c[item].as_bool('enabled'):
                menuitem= c[item]
                menuitem['id'] = item
                if not 'display_name' in menuitem.keys():
                    menuitem['display_name'] = item
                retval.append(menuitem)
        return retval

    def getPluginConfig(self,pluginid):
        """returns the config of a specific plugin.
        This will passed to the plugin constructors"""
        retval =  self.configobj['Menu'][pluginid]
        return retval

    def getGeneralValue(self,key, default=None):
        try:
            retval =  self.configobj['General'][key]
        except:
            logging.error("Problem with loading key from config: %s", key)
            print ("Problem with loading key from config: %s", key)
            retval = default
        return retval
